from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.forms import inlineformset_factory
from django.http import HttpResponse

from .models import *


# Create your views here.
def login_view(request, *args, **kwargs):

    form = UserCreationForm()


    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()

    context = {'form': form}
    return render(request, 'login.html', context)
